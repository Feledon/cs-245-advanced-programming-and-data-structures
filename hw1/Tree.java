package hw1;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public abstract class Tree {
	public abstract long getSize();	
	protected abstract void plot(BufferedImage image, Rectangle rec, boolean isHorizontal);

	public void plot(BufferedImage image){
		plot(image,new Rectangle(image.getWidth(), image.getHeight()), true);		
	}
}
