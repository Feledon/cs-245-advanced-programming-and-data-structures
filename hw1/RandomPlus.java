package hw1;

import java.awt.Color;
import java.util.Random;

@SuppressWarnings("serial")
public class RandomPlus extends Random {
	public int nextInt(int lowNumber, int highNumber){
		highNumber+=1;//To make highNumber inclusive on the Math.random call, add 1 to the super call.
		return super.nextInt(highNumber - lowNumber) + lowNumber;		
	}

	public Color nextColor(int minIntensity){
		return new Color(nextInt(minIntensity, 255),nextInt(minIntensity, 255),nextInt(minIntensity, 255));		
	}
}
