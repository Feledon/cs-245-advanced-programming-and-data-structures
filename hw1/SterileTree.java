package hw1;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public class SterileTree extends Tree{
	private long size = 0;

	public SterileTree(long size){
		this.size = size;
	}

	@Override
	public long getSize() {
		return size;
	}

	@Override
	protected void plot(BufferedImage image, Rectangle rec, boolean isHorizontal) {
		RandomPlus random = new RandomPlus();
		Color color = new Color(random.nextInt(50, 255),random.nextInt(50, 255),random.nextInt(50, 255));		
		Graphics2D g =  image.createGraphics();	
		g.setColor(color);
		g.fill(rec);
	}
}