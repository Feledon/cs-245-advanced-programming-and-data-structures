package hw1;

import java.io.File;
import java.util.Arrays;

public class Main {
	public static Tree buildFileTree(File file){
		if(file.isDirectory()){
			FertileTree tree = new FertileTree();
			File[] newFiles = file.listFiles();
			Arrays.sort(newFiles);
			for(File newFile : newFiles){
				tree.addChild(buildFileTree(newFile));
			}
			return tree;
		}else{
			return new SterileTree(file.length());
		}
	}
}//End Class