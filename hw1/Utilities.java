package hw1;

import java.util.ArrayList;

public class Utilities {

	public static long sum(ArrayList<Long> list){
		long listSum = 0; 
		for(Long tempTree : list){
			listSum = listSum + tempTree;
		}
		return listSum;
	}

	public static ArrayList<Double> proportionalize(ArrayList<Long> list){
		ArrayList<Double> propList = new ArrayList<Double>();
		long listSize = sum(list);

		for(long l : list){
			propList.add((double) l / listSize);
		}

		return propList;
	}

	public static ArrayList<Integer> distribute(ArrayList<Double> proportionList, int amount){
		ArrayList<Integer> distributedList = new ArrayList<Integer>();
		int total = 0;
		for(Double d : proportionList){
			double tempD =  (int) Math.floor(d * amount);
			int temp = (int) tempD;
			total += temp;
			distributedList.add(temp);
		}

		//We have rounded down to 0, so if the end totals don't match add 1 to each of the elements of the array starting at 0, and going until we match.
		if(total != amount){
			for(int i = 0;(i < distributedList.size() && !(total == amount)); i++){
				distributedList.set(i, distributedList.get(i) + 1);
				total++;
			}
		}
		return distributedList;		
	}

}//End Class

