package hw1;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class FertileTree extends Tree {
	private ArrayList<Tree> childTrees = new ArrayList<Tree>();

	public void addChild(Tree tree) {
		childTrees.add(tree);
	}

	public long getSize() {
		long size = 0;
		for(int i = 0; i<childTrees.size(); i++) {
			size = size + childTrees.get(i).getSize();
		}
		return size;
	}

	private ArrayList<Long> getChildSizes(){
		ArrayList<Long> childSizes = new ArrayList<Long>();
		for(int i = 0; i < childTrees.size(); i++) {
			childSizes.add(childTrees.get(i).getSize());
		}
		return childSizes;
	}

	public void plot(BufferedImage image, Rectangle rec, boolean ishorizontal) {
		ArrayList<Double> propSizes = Utilities.proportionalize(getChildSizes());

		int x = (int) rec.getX();
		int y = (int) rec.getY();
		if(ishorizontal) {			
			ArrayList<Integer> horDistributedList = Utilities.distribute(propSizes, (int) rec.getWidth());
			for(int i = 0; i < horDistributedList.size(); i++) {
				childTrees.get(i).plot(image, new Rectangle(x, y, horDistributedList.get(i), (int) rec.getHeight()), !ishorizontal);				
				x += horDistributedList.get(i);
			}
		}else{
			ArrayList<Integer> vertDistributedList = Utilities.distribute(propSizes, (int) rec.getHeight());
			for(int i = 0; i < vertDistributedList.size(); i++) {
				childTrees.get(i).plot(image, new Rectangle(x, y, (int) rec.getWidth(), vertDistributedList.get(i)), !ishorizontal);
				y += vertDistributedList.get(i);
			}
		}
	}
}
