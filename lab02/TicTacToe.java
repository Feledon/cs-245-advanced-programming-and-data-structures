package lab02;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class TicTacToe extends JFrame{

	public static void main(String[] args) {
		JFrame frame = new JFrame("Tic-Tack-Toe");
		JButton firstButton = new JButton();
		JButton secondButton = new JButton();
		JButton thirdButton = new JButton();
		JButton fourthButton = new JButton();
		JButton fifthButton = new JButton();
		JButton sixthButton = new JButton();
		JButton seventhButton = new JButton();
		JButton eightButton = new JButton();
		JButton ninthButton = new JButton();	
		

		
		JLabel topLabel = new JLabel("O's turn");
		topLabel.setHorizontalAlignment(JLabel.CENTER);
		frame.add(topLabel, BorderLayout.NORTH);

		JLabel bottomLabel = new JLabel("To the floor");
		bottomLabel.setHorizontalAlignment(JLabel.CENTER);
		frame.add(bottomLabel, BorderLayout.SOUTH);

		
		JPanel panel = new JPanel();
		GridLayout layout = new GridLayout(3,3);
		panel.setLayout(layout);
		panel.add(firstButton);
		panel.add(secondButton);
		panel.add(thirdButton);
		panel.add(fourthButton);
		panel.add(fifthButton);
		panel.add(sixthButton);
		panel.add(seventhButton);
		panel.add(eightButton);
		panel.add(ninthButton);
		frame.add(panel, BorderLayout.CENTER);


		ButtonClick firstB = new ButtonClick(topLabel, firstButton,panel);
		firstButton.addActionListener(firstB);
		
		ButtonClick secondB = new ButtonClick(topLabel, secondButton,panel);
		secondButton.addActionListener(secondB);
		
		ButtonClick thirdB = new ButtonClick(topLabel, thirdButton,panel);
		thirdButton.addActionListener(thirdB);
		
		ButtonClick fourthB = new ButtonClick(topLabel, fourthButton,panel);
		fourthButton.addActionListener(fourthB);
		
		ButtonClick fifthB = new ButtonClick(topLabel, fifthButton,panel);
		fifthButton.addActionListener(fifthB);
		
		ButtonClick sixthB = new ButtonClick(topLabel, sixthButton,panel);
		sixthButton.addActionListener(sixthB);
		
		ButtonClick seventhB = new ButtonClick(topLabel, seventhButton,panel);
		seventhButton.addActionListener(seventhB);
		ButtonClick eigthB = new ButtonClick(topLabel, eightButton,panel);
		eightButton.addActionListener(eigthB);
		
		ButtonClick ninthB = new ButtonClick(topLabel, ninthButton,panel);
		ninthButton.addActionListener(ninthB);
		
		
		frame.setSize(700, 700);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}


}
