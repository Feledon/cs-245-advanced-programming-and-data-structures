package lab02;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ButtonClick implements ActionListener{

	private JLabel label;
	private JButton button;
	private JPanel panel;

	public ButtonClick(JLabel newLabel, JButton thisButton, JPanel thisPanel){
		label = newLabel;
		button = thisButton;
		panel = thisPanel;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String whosTurn = label.getText().substring(0,1).trim();

		button.setText(whosTurn);

		if(CheckForWin(whosTurn)){
			label.setText(whosTurn + " wins!");
		}else{
			label.setText(whosTurn + "'s turn");	
			button.setEnabled(false);
			if(whosTurn.equalsIgnoreCase("O")){
				whosTurn = "X";
			}else{
				whosTurn = "O";
			}

			label.setText(whosTurn + "'s turn");	
			button.setEnabled(false);
		}
		


	}

	private boolean CheckForWin(String whosTurn){
		JButton[] bA = new JButton[panel.getComponentCount()]; 

		
		int i;
		for (i=0;i<panel.getComponentCount();i++) {
			//index for win [0,1,2]		
			JButton b = (JButton) panel.getComponent(i);
			bA[i] = b;		
		}

		if(bA[0].getText().equalsIgnoreCase(whosTurn)&&bA[1].getText().equalsIgnoreCase(whosTurn)&&bA[2].getText().equalsIgnoreCase(whosTurn)){
			for (i=0;i<panel.getComponentCount();i++) {
				//index for win [0,1,2]		
				JButton b = (JButton) panel.getComponent(i);
				b.setEnabled(false);		
			}
			return true;
		}
		
		return false;
	}


}
