package aplay;

import java.util.Stack;

public class ReverseString {

	private static String reverseString(String start){
		Stack<String> stack = new Stack<String>();
		String reversed = "";

		for(int i = 0; i < start.length(); ++i){
			stack.push(start.substring(i, i + 1));
		}

		while(!stack.isEmpty()){
			reversed += stack.pop();
		}
		return reversed;
	}

	public static void main(String[] args) {
		String start = "Plethora";
		String output = "";

		output = reverseString(start);

		System.out.println(start + " to " + output);	
	}

}
