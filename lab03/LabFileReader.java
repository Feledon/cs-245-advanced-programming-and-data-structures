package lab03;

import java.io.File;

public class LabFileReader {
	
	public static void main(String[] args) {
		listLarge("C:\\Altera");
		//listImages("H:\\Documents\\My Pictures\\Pictures");
	}


	/**
	 * Write a method named listLarge that accepts a path to a directory as a parameter. 
	 * It prints to the console all files in the directory that are larger than some threshold number of bytes.
	 * @param filePath
	 */
	public static void listLarge(String path){
		File dir = new File(path);
		LargeFileFilter filter = new LargeFileFilter();	
		File[] fileList = dir.listFiles(filter);		
		
		for(int i =0; i<fileList.length;i++){
			System.out.println(fileList[i].getName());
		}
	}

	/**
	 * Write a method named listImages that accepts a path to a directory as a parameter. 
	 * It prints to the console all files in the directory that are JPEGs, PNGs, BMPs, or GIFs.
	 * @param imagePath
	 */
	public static void listImages(String path){
		File dir = new File(path);
		ImageFileFilter filter = new ImageFileFilter();	
		File[] fileList = dir.listFiles(filter);		
		
		for(int i =0; i<fileList.length;i++){
			System.out.println(fileList[i].getName());
		}
	}

}
