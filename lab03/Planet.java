package lab03;

public class Planet {
	private double au;
	private double mass;
	private String name;
	public String getName() {
		return name;
	}

	public double getAu() {
		return au;
	}

	public double getMass() {
		return mass;
	}


	
	public Planet (String newName, double newAu, double newMass) {
		name = newName;
		au = newAu;
		mass = newMass;
	}
	
	
	public String toString(){
		return "Name: "+name+" - AU: "+au+" - Mass: "+mass;
	}
}
