package lab03;

import java.util.Comparator;

public class PlanetCompareMass implements Comparator<Planet> {

	public int compare(Planet first, Planet second) {		 
		if(first.getMass() > second.getMass()){
			return 1;
		}else if (first.getMass() < second.getMass()){
			return -1;
		}else
			return 0;
	}
}