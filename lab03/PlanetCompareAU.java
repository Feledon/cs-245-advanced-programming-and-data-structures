package lab03;

import java.util.Comparator;

public class PlanetCompareAU implements Comparator<Planet>{

	@Override
	public int compare(Planet first, Planet second) {
		
		/**
		 * Returns:a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater than the second.
		 */
		if(first.getAu() > second.getAu()){
			return 1;
		}else if (first.getAu() < second.getAu()){
			return -1;
		}else
		return 0;
	}
	
	

}
