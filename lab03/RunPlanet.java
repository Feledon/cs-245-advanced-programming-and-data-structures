package lab03;

import java.util.Arrays;

public class RunPlanet {

	public static void main(String[] args) {
		Planet[] planets = buildArray();
		
		System.out.println("Sorted by AU highest to lowest");
		PlanetCompareAU compAU = new PlanetCompareAU();
		Arrays.sort(planets, compAU);		
		for(int i =0; i<planets.length;i++){
			System.out.println(planets[i].toString());
		}
		
		System.out.println("Sorted by Mass highest to lowest");		
		PlanetCompareMass compMass = new PlanetCompareMass();
		Arrays.sort(planets, compMass);		
		for(int i =0; i<planets.length;i++){
			System.out.println(planets[i].toString());
		}
	}

	
	/**
	 * Populate the Array of Planet objects.
	 * @return Planet[]
	 */
	private static Planet[] buildArray(){
		Planet[] planets = {
				new Planet("Saturn", 9.5, 95.0),
				new Planet("Earth", 1.0, 1.0),
				new Planet("Venus", 0.7, 0.815),
				new Planet("Jupiter", 5.2, 318.0),
				new Planet("Uranus", 19.6, 14.0),
				new Planet("Neptune", 30.0, 17.0),
				new Planet("Mars", 1.5, 0.107),
				new Planet("Mercury", 0.4, 0.055)
		};
		return planets;		
	}
}
