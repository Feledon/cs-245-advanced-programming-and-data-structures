package lab05;

import junit.framework.Assert;
import org.junit.Test;

/**
 * History will have a constructor that takes in an int for its size, 
 * 
 * an add method that takes in an instance of type T to add, 
 * 
 * and a get method that takes in an int. 
 * A call to get(0) returns the most recently added item, get(1) the second most recently added, get(2) the third most recently added, and so on.
 * @author gehrkeb
 *
 */
public class TestHistory {

	@Test
	public void testConstructor(){		
		History<String> hist = new History<String>(3);
		
		Assert.assertNotNull(hist);
	}
	
	@Test
	public void testGet(){
		History<String> hist = new History<String>(3);
		hist.add("dog");
		hist.add("cat");
		hist.add("bird");
		
		Assert.assertEquals("dog", hist.get(2));
		Assert.assertEquals("cat", hist.get(1));
		Assert.assertEquals("bird", hist.get(0));
	}
	
	@Test
	public void testAdd(){
		History<String> hist = new History<String>(3);
		hist.add("dog");
		hist.add("cat");
		hist.add("bird");
		
		Assert.assertEquals("dog", hist.get(2));
		Assert.assertEquals("cat", hist.get(1));
		Assert.assertEquals("bird", hist.get(0));
	}
	
	@Test (expected = IndexOutOfBoundsException.class)
	public void testFailure(){		
		History<String> hist = new History<String>(3);
		hist.add("dog");
		hist.add("cat");
		
		Assert.assertEquals("dog", hist.get(1));		
		hist.get(2);	
		
	}

}
