package lab05;

/**
 * 
 * If someone tries to get the nth item back in the history, but fewer than n items have been added, throw an IndexOutOfBoundsException.
 There are various ways to solve this problem. Some require looping, and some do not require any loops. Choose an implementation that seems sound to you.

 History will have a constructor that takes in an int for its size, 
 * 
 * an add method that takes in an instance of type T to add, 
 * 
 * and a get method that takes in an int. 
 * A call to get(0) returns the most recently added item, get(1) the second most recently added, get(2) the third most recently added, and so on.
 * @author gehrkeb
 *
 */


public class History<T> {

	private T[] items;
	private int nitems;

	public History (int size) {
		items = (T[]) new Object[size];
		nitems = 0;
	}

	public void add(T item) {
		items[nitems] = item;
		++nitems;

	}

	public T get(int index) {
		if (nitems <= index) {
			throw new IndexOutOfBoundsException("We dont have that many of things.");
		}

		return items[nitems - index - 1];
	}
}
