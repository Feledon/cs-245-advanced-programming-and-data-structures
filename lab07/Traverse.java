package lab07;

import java.awt.Point;

public class Traverse {

	public static void main(String[] args) {

		Maze testMaze = new Maze(40, 40, 40);

		MazeFrame frameh = new MazeFrame(testMaze);

		Point start = new Point (1, 1);

		Queue<Point> bookMarkCollectionatorizerCuzIntellisenseisDead = new Queue();

		bookMarkCollectionatorizerCuzIntellisenseisDead.enqueue(start);

		int count = 1;
		while(!testMaze.isEscaped() && !bookMarkCollectionatorizerCuzIntellisenseisDead.isEmpty() ){
			start = bookMarkCollectionatorizerCuzIntellisenseisDead.dequeue();
			testMaze.visitCell(start.x, start.y);
			frameh.repaintNow();
			if( testMaze.getUnvisitedNeighborsCount(start.x, start.y) > 1){
				bookMarkCollectionatorizerCuzIntellisenseisDead.enqueue(start);
			}

			if( testMaze.isUnvisited(start.x -1, start.y)){
				bookMarkCollectionatorizerCuzIntellisenseisDead.enqueue(new Point (start.x -1, start.y));
				++count;
			}else if( testMaze.isUnvisited(start.x+1, start.y)){
				bookMarkCollectionatorizerCuzIntellisenseisDead.enqueue(new Point (start.x +1, start.y)); 
				++count;
			}else if( testMaze.isUnvisited(start.x, start.y-1)){
				bookMarkCollectionatorizerCuzIntellisenseisDead.enqueue(new Point (start.x, start.y-1));
				++count;
			}else if( testMaze.isUnvisited(start.x, start.y+1)){
				bookMarkCollectionatorizerCuzIntellisenseisDead.enqueue(new Point (start.x, start.y+1));
				++count;
			}
		}
		System.out.println("Count: " + count);

	}

}
