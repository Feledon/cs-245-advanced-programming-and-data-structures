package lab07;

import java.util.LinkedList;

public class Stack<T> {
  private LinkedList<T> items;
  
  public Stack() {
    items = new LinkedList<T>();
  }
  
  public boolean isEmpty() {
    return items.isEmpty();
  }

  public int size() {
    return items.size();
  }
  
  public void push(T itemToAdd) {
    items.add(itemToAdd);
  }
  
  public T pop() {
    return items.removeLast();
  }
  
  public T peek() {
    return items.getLast();
  }
  
//  public void push(T itemToAdd) {
//    super.pu
//  }
  
  public static void main(String[] args) {
    Queue<Integer> numbers = new Queue<Integer>();
    
    numbers.enqueue(10);
    numbers.enqueue(25);
//    process(numbers);
//    numbers.add(0, 20);
    
    System.out.println(numbers.peek());
    System.out.println(numbers.dequeue());
    System.out.println(numbers.peek());
  }
  
//  public static void process(LinkedList<Integer> l) {
//    l.add(0, 56);
//  }
}