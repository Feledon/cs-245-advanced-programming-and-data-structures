package lab07;

import java.util.LinkedList;

public class Queue<T> {
	private LinkedList<T> items;

	public Queue() {
		items = new LinkedList<T>();
	}

	public boolean isEmpty() {
		return items.isEmpty();
	}

	public int size() {
		return items.size();
	}

	public void enqueue(T itemToAdd) {
		items.add(itemToAdd);
	}

	public T dequeue() {
		return items.removeFirst();
	}
	public T peek() {
		return items.getFirst();
	}
}
