package lab06;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;


public class ListComparison {
	private static int LIST_SIZE = 10000000;

	public static void main(String[] args) {
		DecimalFormat df = new DecimalFormat("#.##");
		LinkedList<Integer> lList = new LinkedList<Integer>();
		long time = timedInsertRandom(lList, true);
		System.out.println("LinkedList");
		System.out.println(df.format((double)time/1000000000.0) + " seconds or " + time + "\n");
		
//		System.out.println("LinkedList PreAppend");
//		System.out.println(timedInsertRandom(lList, false)/1000000000 + "\n");
//		
		
		
		ArrayList<Integer> aList = new ArrayList<Integer>();
		System.out.println("ArrayList");
		time = timedInsertRandom(aList, true);
		System.out.println(df.format((double)time/1000000000.0) + " seconds or " + time + "\n");

//		System.out.println("ArrayList PreAppend");
//		System.out.println(timedInsertRandom(aList, false)/1000000000);

	}

	public static List<Integer> insertRandom(List<Integer> intList, boolean isAppend) {
		Random rand = new Random();
		rand.setSeed(1001);

		for(int i = 0; i < LIST_SIZE; i++) {
			if(isAppend){
				intList.add(rand.nextInt());
			}else{
				intList.add(0, rand.nextInt());
			}			
		}
		return intList;
	}

	public static long timedInsertRandom(List<Integer> intList, boolean isAppend) {
		ThreadMXBean timer = ManagementFactory.getThreadMXBean();
		long start = timer.getCurrentThreadCpuTime();

		// repeatedly fill the list
		List<Integer> newList = insertRandom(intList, isAppend);
		Iterator<Integer> it = newList.iterator();
		int sum = 0;
		while(it.hasNext()){
			sum += it.next();
		}

		long end = timer.getCurrentThreadCpuTime();
		long nNanos = end - start;

		return nNanos;
	}

}