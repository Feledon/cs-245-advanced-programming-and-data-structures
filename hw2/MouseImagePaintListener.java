package hw2;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class MouseImagePaintListener implements MouseListener{
	private BufferedImage image;
	private JFrame frame;

	public MouseImagePaintListener(BufferedImage newImage, JFrame newFrame){
		image = newImage;
		frame = newFrame;
	}

	@Override
	public void mouseClicked(MouseEvent arg) {
		if(arg.getX() < image.getWidth() && arg.getX() > -1 && arg.getY() < image.getHeight() && arg.getY() > - 1){
			Color oldColor = new Color(image.getRGB(arg.getX(), arg.getY()));
			if(arg.getButton() ==  MouseEvent.BUTTON1){
				try{
					ImageUtilities.floodFillRecursive(arg.getX(), arg.getY(), oldColor, Color.CYAN, image);
				} catch (StackOverflowError e) {
					JOptionPane.showMessageDialog(null, "Too much data for the Recursive method.");
				}
			}else{
				ImageUtilities.floodFillIterative(arg.getX(), arg.getY(), oldColor, Color.RED, image);
			}
			frame.repaint();
		}
	}	

	@Override
	public void mouseReleased(MouseEvent arg0) {
	}

	@Override
	public void mousePressed(MouseEvent arg) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
	}

}
