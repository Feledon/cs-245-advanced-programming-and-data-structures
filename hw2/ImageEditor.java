package hw2;

import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class ImageEditor extends JFrame{
	private JFrame frame = new JFrame();
	private JLabel imageLabel = new JLabel();
	private JPanel panel = new JPanel();
	
	public static void main(String[] args) {
		ImageEditor ie = new ImageEditor();
	}	
	
	public ImageEditor(){		
		frame = new JFrame("Flood Filling Fun Festival");
		JMenuBar menuBar = new JMenuBar();
		JMenu menu;
		JMenuItem menuItem;		
		menu = new JMenu("File");
		menuItem = new JMenuItem("New Image...",KeyEvent.VK_T);
		menu.add(menuItem);		
		menuBar.add(menu);

		menuItem.addActionListener(new ImageChangedEvent(this));

		frame.setJMenuBar(menuBar);			
		frame.setSize(500,500);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void setImage(final BufferedImage image){
		panel.remove(imageLabel);
		frame.setSize(image.getWidth() + 25, image.getHeight() + 100); //Adjust size for the menu bar

		MouseImagePaintListener listener = new MouseImagePaintListener(image, frame);
		panel.addMouseListener(listener);

		imageLabel = new JLabel(new ImageIcon(image));
		imageLabel.setHorizontalAlignment(JLabel.CENTER);

		panel.add(imageLabel, BorderLayout.CENTER);
		frame.add(panel,BorderLayout.CENTER);
	}
}
