package hw2;

import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.Stack;

public class ImageUtilities {
	public static void floodFillRecursive(int x, int y, Color startColor, Color newColor, BufferedImage image){
		if(!startColor.equals(newColor)){
			if(x < image.getWidth() && x > -1 && y < image.getHeight() && y > -1){
				Color imagePixel = new Color(image.getRGB(x, y)); 
				if(imagePixel.equals(startColor)){
					image.setRGB(x, y, newColor.getRGB());
					floodFillRecursive(x + 1, y, startColor, newColor, image);
					floodFillRecursive(x - 1, y, startColor, newColor, image);
					floodFillRecursive(x, y - 1, startColor, newColor, image);
					floodFillRecursive(x, y + 1, startColor, newColor, image);
				}
			}
		}	
	}
 
	public static void floodFillIterative(int x, int y, Color startColor, Color newColor, BufferedImage image){
		Stack<Point> stack = new Stack<Point>();
		if(!startColor.equals(newColor)){
			stack.push(new Point(x,y));
			while(!stack.isEmpty()){
				Point point = stack.pop();
				while(isValidY(point, image) && isColorMatch(point, startColor, image)){	
					boolean leftScanlineAlreadyPushed = false;
					boolean rightScanlineAlreadyPushed = false;
					point = new Point(point.x, point.y + 1);
					Point newPoint= new Point(point.x, point.y - 1);

					while(isValidY(newPoint, image) && isColorMatch(newPoint, startColor, image)){					
						image.setRGB(newPoint.x, newPoint.y, newColor.getRGB());
						Point left = new Point(newPoint.x - 1, newPoint.y);
						Point right = new Point(newPoint.x + 1, newPoint.y);
						newPoint = new Point(newPoint.x, newPoint.y - 1);

						if(isValidX(left, image)){
							if(!leftScanlineAlreadyPushed && isColorMatch(left, startColor, image)){
								stack.push(left);
								leftScanlineAlreadyPushed = true;
							}else if(leftScanlineAlreadyPushed && !isColorMatch(left, startColor, image)){
								leftScanlineAlreadyPushed = false;
							}
						}
						if(isValidX(right, image)){
							if(!rightScanlineAlreadyPushed && isColorMatch(right, startColor, image)){
								stack.push(right);
								rightScanlineAlreadyPushed = true;
							}else if(rightScanlineAlreadyPushed && !isColorMatch(right, startColor, image)){
								rightScanlineAlreadyPushed = false;
							}
						}
					}			
				}
			}
		}
	}	

	private static boolean isValidY(Point point, BufferedImage image){
		return (point.y > -1 && point.y < image.getHeight());	
	}
	private static boolean isValidX(Point point, BufferedImage image){
		return (point.x > -1 && point.x < image.getWidth());
	}

	private static boolean isColorMatch(Point point, Color startColor, BufferedImage image){
		Color imagePixel = new Color(image.getRGB(point.x, point.y));
		return (imagePixel.equals(startColor));	
	}
}
