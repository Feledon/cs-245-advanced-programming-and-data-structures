package hw2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public class ImageChangedEvent implements ActionListener {
private ImageEditor iEditor;
	
	public ImageChangedEvent(ImageEditor newIEditor){
		this.iEditor = newIEditor;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		iEditor.setImage(getImage());	
	}

	private BufferedImage getImage(){
		BufferedImage img = null;
		FileNameExtensionFilter filter = new FileNameExtensionFilter("JPG, PNG & GIF Images", "jpg", "gif","png");
		JFileChooser fc = new JFileChooser();
		try {
			fc.setFileFilter(filter);
			fc.showOpenDialog(iEditor);
			img = ImageIO.read(fc.getSelectedFile());
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Error reading the file.");
		}

		return img;
	}
}
