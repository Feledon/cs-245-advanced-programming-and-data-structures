package pre1;

public class Scale implements Transform {
	private double scaleInX;
	private double scaletInY;

	@Override
	public Point apply(Point oldPoint) {
		return new Point(oldPoint.getX() * scaleInX, oldPoint.getY() * scaletInY);
	}


	public Scale(double x, double y){
		scaleInX = x;
		scaletInY = y;
	}
}//End Class