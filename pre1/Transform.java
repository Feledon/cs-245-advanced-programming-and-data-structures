package pre1;

public interface Transform {
	public Point apply(Point oldPoint);	
}
