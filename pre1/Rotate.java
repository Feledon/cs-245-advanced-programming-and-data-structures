package pre1;

public class Rotate implements Transform {
	private double degree;

	@Override
	public Point apply(Point oldPoint) {
		double x = oldPoint.getX();
		double y = oldPoint.getY();
		double c = Math.cos(Math.toRadians(degree));
		double s = Math.sin(Math.toRadians(degree));

		double newX = (x*c)-(y*s);
		double newY = (x*s)+(y*c);

		return new Point(newX, newY);
	}

	public Rotate(double tempAngle){
		degree = tempAngle;		
	}
}//End Class
