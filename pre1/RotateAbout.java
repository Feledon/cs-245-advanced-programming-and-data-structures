package pre1;

public class RotateAbout implements Transform {
	private double degrees = 0.0;
	private Point pivotPoint = new Point(); 

	@Override
	public Point apply(Point victimPoint) {
		Translate translateToOrigin = new Translate(-pivotPoint.getX(), -pivotPoint.getY());
		Rotate newRotate = new Rotate(degrees);
		Translate translateFromOrigin = new Translate(pivotPoint.getX(), pivotPoint.getY());

		victimPoint = victimPoint.transform(translateToOrigin);
		victimPoint = victimPoint.transform(newRotate);
		victimPoint = victimPoint.transform(translateFromOrigin);

		return victimPoint;
	}

	public RotateAbout(Point tempPoint, double tempDegrees){
		degrees = tempDegrees;
		pivotPoint = tempPoint;
	}


}//End Class
