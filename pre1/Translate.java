package pre1;

public class Translate implements Transform {
	private double shiftInX;
	private double shiftInY;

	@Override
	public Point apply(Point oldPoint) {
		return new Point(oldPoint.getX() + shiftInX, oldPoint.getY() + shiftInY);
	}

	public Translate(double x, double y){		
		shiftInX = x;
		shiftInY = y;
	}
}//end class
