package pre1;

public class Point {
	private double x;
	public double getX() {
		return x;
	}
	private double y;
	public double getY() {
		return y;
	}

	public Point(){
		x=0;
		y=0;
	}

	public Point(double tempX, double tempY){
		x=tempX;
		y=tempY;
	}

	public Point transform(Transform oldPoint){
		return oldPoint.apply(new Point(x,y));			
	}
}//End Class
