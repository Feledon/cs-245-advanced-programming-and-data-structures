package lab08;

import hw1.RandomPlus;

import java.awt.Color;
import java.awt.image.BufferedImage;

public class MyThreadRunnable implements Runnable {
	private int threadIndex;
	private int threadCount;
	private BufferedImage readImage;
	private BufferedImage writeImage;


	public MyThreadRunnable(BufferedImage readImage, BufferedImage writeImage, int threadIndex, int threadCount){
		this.readImage = readImage;
		this.writeImage = writeImage;
		this.threadIndex = threadIndex;
		this.threadCount = threadCount;	
	}

	@Override
	public void run() {
		RandomPlus random = new RandomPlus();
		Color color = new Color(random.nextInt(50, 255),random.nextInt(50, 255),random.nextInt(50, 255));
System.out.println(threadIndex);
		int	colWidth = readImage.getWidth()/threadCount;
		int	startX = colWidth * threadIndex;

		for(int x = startX; x < (startX + colWidth); x++){
			for(int y = 0; y < readImage.getHeight();y++){
				writeImage.setRGB(x, y, color.getRGB());
			}
		}
	}
}

