package lab08;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;



public class ThreadStarter {
	public static void main(String[] args) throws IOException {
		File file = new File("H:\\Documents\\My Pictures\\Untitled.jpg");
		BufferedImage start = ImageIO.read(file);
		BufferedImage finish = new BufferedImage(start.getWidth(), start.getHeight(), start.getType());
				
//	    Thread a = new Thread(new MyThreadRunnable(start, finish, 0, 3));
//	    Thread b = new Thread(new MyThreadRunnable(start, finish, 1, 3));
//	    Thread c = new Thread(new MyThreadRunnable(start, finish, 2, 3));

	    ArrayList<Thread> list = new ArrayList<Thread>();
	    int size = 20;
	    for(int i = 0; i < size; i++){
	    	list.add(new Thread(new MyThreadRunnable(start, finish, i, size)));
	    }
	    
	    // Start the threads going concurrently.
//	    a.start();
//	    b.start();
//	    c.start();
	    for(int i = 0; i < list.size(); i++){
	    	list.get(i).start();
	    }

	    // Wait for them all to finish.
	    try {
//	      a.join();
//	      b.join();
//	      c.join();
		    for(int i = 0; i < list.size(); i++){
		    	list.get(i).join();
		    }
	    } catch (InterruptedException e) {
	    	 System.out.println("Broke.");
	    }
	    ImageIO.write(finish, "png", new File("H:\\Documents\\My Pictures\\Untitled2.jpg"));
	    System.out.println("All threads have finished.");
	}
}
