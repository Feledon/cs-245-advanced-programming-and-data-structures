package lab08;

public class ThreadEcho {
	  public static void main(String[] args) {
	    Thread a = new Thread(new Echoer(0));
	    Thread b = new Thread(new Echoer(1));
	    Thread c = new Thread(new Echoer(2));

	    // Start the threads going concurrently.
	    a.start();
	    b.start();
	    c.start();

	    // Wait for them all to finish.
	    try {
	      a.join();
	      b.join();
	      c.join();
	    } catch (InterruptedException e) {
	    }

	    System.out.println("All threads have finished.");
	  }

	  private static class Echoer implements Runnable {
	    private int threadIndex;

	    public Echoer(int threadIndex) {
	      this.threadIndex = threadIndex;
	    }

	    @Override
	    public void run() {
	      int nMillis = (int) (Math.random() * 1000 * 20);
	      System.out.println("Thread " + threadIndex + " started and will sleep for " + nMillis / 1000.0 + " seconds.");
	      try {
	        Thread.sleep(nMillis);
	      } catch (InterruptedException e) {
	        // Sleep ended prematurely. Oh, well.
	      }
	      System.out.println("Thread " + threadIndex + " finished.");
	    }
	  }
	}