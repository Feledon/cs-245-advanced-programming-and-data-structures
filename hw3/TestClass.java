package hw3;

import hw3.LSystem.LSystemEvolution;

import java.io.File;
import java.io.IOException;
import java.util.Set;

public class TestClass {
	public static void main(String[] args) throws IOException {
//		LSystem test = new LSystem("F", 0);
		LSystem test = new LSystem(new File("H:\\CS 245\\test.txt"));

//		System.out.println(test.getIntitalState());
//		System.out.println(test.getTheta());

//		test.addProduction('F', "F-F");
//		System.out.println(test.getProduction('F'));
//		test.addProduction('F', "F+F-F");
//		test.addProduction('E', "F+F-F");
//		System.out.println(test.getProductionKeys().toString());
//		System.out.println(test.getProduction('F'));
//		test.removeProduction('E');
		
//		System.out.println(test.getProductionKeys().toString());
		
		LSystem lsystem = new LSystem("F", 90);
		lsystem.addProduction('F', "F-FF");
		LSystemEvolution evolution = lsystem.new LSystemEvolution();
		evolution.evolve();
		System.out.println(" first :" + evolution.getStage());		
		evolution.evolveTo(4);	
		System.out.println(" second :" + evolution.getStage());
//		System.out.println("stage :" + evolution.getStage() + " = " +evolution.toString());
	}
}
