package hw3;

import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;
import java.util.Stack;

public class LSystem{
	private HashMap<Character, String> productions;
	private String initialState;
	private double theta;

	public String getInitialState() {
		return initialState;
	}
	public void setInitialState(String intitalState) {
		this.initialState = intitalState;
	}
	public double getTheta() {
		return theta;
	}
	public void setTheta(double theta) {
		this.theta = theta;
	}

	public LSystem(String intitalState, double theta){
		this.initialState = intitalState;
		this.theta = theta;	
		productions = new HashMap<Character, String>();
	}

	public LSystem(File file) throws FileNotFoundException{
		Scanner in = new Scanner(file);
		int line = 0;
		productions = new HashMap<Character, String>();

		while (in.hasNext()) {
			String word = in.next();
			if(line==0){
				this.initialState= word; 
				this.theta = in.nextDouble();		      
			}else{
				productions.put(word.charAt(0), in.next());
			}
			++line;
		}
		in.close();
	}

	public void addProduction(char key, String replacement){
		productions.put(key, replacement);
	}

	public String getProduction(char key){
		String s = key + "";
		if(productions.containsKey(key)){
			s = productions.get(key);
		}
		return s;
	}

	public void removeProduction(char key){
		if(productions.containsKey(key)){
			productions.remove(key);
		}
	}

	public Set<Character> getProductionKeys(){
		return productions.keySet();
	}


	/**
	 * Inner class for handling evolutions
	 * @author gehrkeb
	 *
	 */
	public class LSystemEvolution{
		private int stage;
		private String state;
		public int getStage(){
			return stage;
		}
		public String toString(){
			return state;
		}
		public LSystemEvolution(){
			this.stage = 0;
			this.state = initialState;
		}

		public void evolve(){
			String out = "";
			for(char c : state.toCharArray()){
				out += getProduction(c);
			}
			state = out;
			++stage;
		}

		public void evolveTo(int stage){
			if(stage < 0){
				throw new IllegalArgumentException();
			}	
			if(stage < this.stage){//Re-evolve the whole damn thing.
				this.stage = 0;
				this.state = initialState;
			}
			for(int i = this.stage; i < stage; i++){
				this.evolve();
			}
		}

		public void reevolve(){
			int currentStage = stage;
			stage = 0;
			this.state = initialState;
			evolveTo(currentStage);
		}

		public Rectangle2D.Double draw(Graphics2D image){
			Turtle turtle = new Turtle(0, 0, 90.0);
			Rectangle2D.Double  dim = new Rectangle2D.Double();
			double minX = 0.0;
			double minY = 0.0;
			double maxX = 0.0;
			double maxY = 0.0;
			Stack<Turtle> turtleStack = new Stack<Turtle>();

			for(char c : state.toCharArray()){
				double baseX = turtle.getX();
				double baseY = turtle.getY();

				if( c == '+'){
					turtle.setHeading(turtle.getHeading() - getTheta());					
				}else if( c == '-'){
					turtle.setHeading(turtle.getHeading() + getTheta());
				}else if( c == 'f' || c == 'g'){
					turtle.setX(turtle.getX() + Math.cos(Math.toRadians(turtle.getHeading())));
					turtle.setY(turtle.getY() + Math.sin(Math.toRadians(turtle.getHeading())));
				}else if( c == 'F' || c == 'G'){
					turtle.setX(turtle.getX() + Math.cos(Math.toRadians(turtle.getHeading())));					
					turtle.setY(turtle.getY() + Math.sin(Math.toRadians(turtle.getHeading())));
					image.draw(new Line2D.Double(turtle.getX(), turtle.getY(), baseX, baseY));
				}else if( c == '['){
					turtleStack.push(turtle);
				}else if( c == ']'){
					turtle = turtleStack.pop();//pop a turtle off the stack and use it for subsequent drawings
				}
				if(minX > turtle.getX()){
					minX = turtle.getX();
				}
				if(maxX < turtle.getX()){
					maxX = turtle.getX();
				}
				if(minY > turtle.getY()){
					minY = turtle.getY();
				}
				if(maxY < turtle.getY()){
					maxY = turtle.getY();
				}
			}
			dim.setFrameFromDiagonal(minX, minY, maxX, maxY);			
			return dim;
		}//End draw
	}
}
