package hw3;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class LSystemViewer extends JFrame {
	private LSystem lsystem;
	private LSystem.LSystemEvolution evolution;
	private LSystemViewerCanvas canvas;
	private LSystemViewerControlPanel controlPanel;

	public LSystemViewer() {
		super("L-System Viewer");

		canvas = new LSystemViewerCanvas();
		controlPanel = new LSystemViewerControlPanel();

		JSplitPane splitPane;
		splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, canvas, controlPanel);
		splitPane.setResizeWeight(1.0);

		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		menuBar.add(fileMenu);

		JMenuItem openItem = new JMenuItem("Open", KeyEvent.VK_O);
		openItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.META_MASK));
		fileMenu.add(openItem);

		setJMenuBar(menuBar);

		openItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				JFileChooser chooser = new JFileChooser();
				int status = chooser.showOpenDialog(LSystemViewer.this);
				if (status == JFileChooser.APPROVE_OPTION) {
					try {
						setLSystem(new LSystem(chooser.getSelectedFile()));
					} catch (Exception e) {
						JOptionPane.showMessageDialog(LSystemViewer.this, "File couldn't be read for one reason or another. Does the file exist? Is it properly formatted?", "Input Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});

		add(splitPane, BorderLayout.CENTER);
		setSize(new Dimension(512, 512));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

	public void setLSystem(LSystem lsystem) {
		this.lsystem = lsystem;
		evolution = lsystem.new LSystemEvolution();
		controlPanel.fillGUI();
		repaint();
	}

	private class LSystemViewerCanvas extends JPanel implements MouseWheelListener {
		private double scale;
		private Point2D.Double mouseShift;
		private Point mouseAt;

		public LSystemViewerCanvas() {
			mouseShift = new Point2D.Double();
			mouseAt = new Point();

			addMouseWheelListener(this);
			MouseAdapter adapter = new MouseAdapter() {
				public void mousePressed(MouseEvent e) {
					mouseAt.x = e.getX();
					mouseAt.y = e.getY();
				}

				@Override
				public void mouseDragged(MouseEvent e) {
					mouseShift.x += (e.getX() - mouseAt.x) / scale;
					mouseShift.y += (e.getY() - mouseAt.y) / scale;
					mouseAt.x = e.getX();
					mouseAt.y = e.getY();
					repaint();
				}
			};
			addMouseListener(adapter);
			addMouseMotionListener(adapter);
			scale = 1.0;
		}

		@Override
		public void paintComponent(Graphics g) {
			super.paintComponent(g);

			Graphics2D g2 = (Graphics2D) g;
			Rectangle2D.Double bounds = evolution.draw(g2);

			if (bounds.getWidth() < 0.1) {
				bounds.x -= 0.1f;
				bounds.width += 0.2f;
			}

			if (bounds.getHeight() < 0.1) {
				bounds.y -= 0.1f;
				bounds.height += 0.2f;
			}

			g2.setColor(Color.WHITE);
			g2.fillRect(0, 0, getWidth(), getHeight());
			g2.setColor(Color.BLACK);

			final double BORDER = 5.0;
			double windowAspect = getWidth() / (double) getHeight();
			double boundsAspect = bounds.getWidth() / bounds.getHeight();

			g2.translate(0.0f, getHeight());
			g2.scale(1.0, -1.0);
			g2.translate(BORDER, BORDER);

			double r = bounds.getWidth() * 0.5;
			double t = bounds.getHeight() * 0.5;
			if (windowAspect < boundsAspect) {
				t = t / windowAspect * boundsAspect;
			} else {
				r = r * windowAspect / boundsAspect;
			}
			g2.scale((getWidth() - 2.0 * BORDER) / (2.0 * r), (getHeight() - 2.0 * BORDER) / (2.0 * t));
			g2.translate(r, t);

			g2.scale(scale, scale);
			g2.translate(mouseShift.x * (2.0 * r) / getWidth(), mouseShift.y * (2.0 * t) / getHeight());
			g2.translate(-bounds.getCenterX(), -bounds.getCenterY());

			g2.setStroke(new BasicStroke(0.1f));
			evolution.draw(g2);
		}

		public void mouseWheelMoved(MouseWheelEvent e) {
			scale += e.getWheelRotation() * 0.1 * scale;
			repaint();
		}
	}

	private class LSystemViewerControlPanel extends JPanel {
		private JTextField initialField;
		private JTextField angleField;
		private JPanel productionsPanel;
		private JTextArea currentStateLabel;

		public LSystemViewerControlPanel() {
			super(new GridBagLayout());

			// Control panel widgets.
			initialField = new JTextField(20);
			angleField = new JTextField(20);

			final JSpinner evolveSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 20, 1));

			productionsPanel = new JPanel(new GridBagLayout());
			productionsPanel.setBorder(BorderFactory.createTitledBorder("Productions"));

			// Callbacks
			evolveSpinner.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent event) {
					evolution.evolveTo(((SpinnerNumberModel) evolveSpinner.getModel()).getNumber().intValue());
					currentStateLabel.setText(evolution.toString());
					canvas.repaint();
				}
			});

			initialField.getDocument().addDocumentListener(new DocumentListener() {
				public void changedUpdate(DocumentEvent e) {
					notifyDelegate();
				}

				public void insertUpdate(DocumentEvent e) {
					notifyDelegate();
				}

				public void removeUpdate(DocumentEvent e) {
					notifyDelegate();
				}

				private void notifyDelegate() {
					lsystem.setInitialState(initialField.getText());
					evolution.reevolve();
					currentStateLabel.setText(evolution.toString());
					canvas.repaint();
				}
			});

			angleField.getDocument().addDocumentListener(new DocumentListener() {
				public void changedUpdate(DocumentEvent e) {
					notifyDelegate();
				}

				public void insertUpdate(DocumentEvent e) {
					notifyDelegate();
				}

				public void removeUpdate(DocumentEvent e) {
					notifyDelegate();
				}

				private void notifyDelegate() {
					double angle = 0.0;
					try {
						angle = Double.parseDouble(angleField.getText());
					} catch (NumberFormatException e) {
					} finally {
						lsystem.setTheta(angle);
						evolution.reevolve();
						canvas.repaint();
					}
				}
			});

			// Layout
			GridBagConstraints c = new GridBagConstraints();
			c.gridwidth = 1;
			c.gridheight = 1;
			c.insets = new Insets(2, 2, 2, 2);

			// Row 0
			c.gridy = 0;

			c.gridx = 0;
			c.weightx = 0.0;
			c.fill = GridBagConstraints.NONE;
			add(new JLabel("Initial"), c);

			c.gridx = 1;
			c.weightx = 1.0;
			c.fill = GridBagConstraints.HORIZONTAL;
			add(initialField, c);

			// Row 1
			++c.gridy;

			c.gridx = 0;
			c.weightx = 0.0;
			c.fill = GridBagConstraints.NONE;
			add(new JLabel("Angle"), c);

			c.gridx = 1;
			c.weightx = 1.0;
			c.fill = GridBagConstraints.HORIZONTAL;
			add(angleField, c);

			// Row 2
			++c.gridy;

			c.gridx = 0;
			c.gridwidth = 1;
			c.weightx = 0.0;
			add(new JLabel("Stage"), c);

			c.gridx = 1;
			c.weightx = 1.0;
			add(evolveSpinner, c);

			// Row 3
			++c.gridy;

			c.gridx = 0;
			c.gridwidth = 2;
			c.weightx = 1.0;
			add(productionsPanel, c);

			// Row 4
			++c.gridy;
			add(new JLabel("Current Evolution"), c);

			// Row 5
			++c.gridy;

			c.gridx = 0;
			c.gridwidth = 2;
			c.weightx = 1.0;
			c.weighty = 1.0;
			c.fill = GridBagConstraints.BOTH;
			currentStateLabel = new JTextArea();
			currentStateLabel.setLineWrap(true);

			JScrollPane scroller = new JScrollPane(currentStateLabel);

			add(scroller, c);
		}

		private void fillGUI() {
			initialField.setText(lsystem.getInitialState());
			angleField.setText("" + lsystem.getTheta());
			currentStateLabel.setText(evolution.toString());

			productionsPanel.removeAll();
			productionsPanel.revalidate();
			GridBagConstraints c = new GridBagConstraints();
			c.gridwidth = 1;
			c.gridheight = 1;
			c.insets = new Insets(2, 2, 2, 2);
			c.gridy = 0;

			for (Character key : lsystem.getProductionKeys()) {
				// Widgets
				final JTextField keyField = new JTextField("" + key, 3);
				keyField.setColumns(10);
				final JTextField valueField = new JTextField(lsystem.getProduction(key));
				JButton deleteButton = new JButton("x");

				// Event Handlers
				final char keyUnderEdit = key;
				deleteButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						lsystem.removeProduction(keyUnderEdit);
						evolution.reevolve();
						canvas.repaint();
						fillGUI();
					}
				});

				valueField.getDocument().addDocumentListener(new DocumentListener() {
					public void changedUpdate(DocumentEvent e) {
						notifyDelegate();
					}

					public void insertUpdate(DocumentEvent e) {
						notifyDelegate();
					}

					public void removeUpdate(DocumentEvent e) {
						notifyDelegate();
					}

					private void notifyDelegate() {
						lsystem.addProduction(keyUnderEdit, valueField.getText());
						evolution.reevolve();
						canvas.repaint();
					}
				});

				keyField.getDocument().addDocumentListener(new DocumentListener() {
					public void changedUpdate(DocumentEvent e) {
						notifyDelegate();
					}

					public void insertUpdate(DocumentEvent e) {
						notifyDelegate();
					}

					public void removeUpdate(DocumentEvent e) {
						notifyDelegate();
					}

					private void notifyDelegate() {
						lsystem.removeProduction(keyUnderEdit);
						lsystem.addProduction(keyField.getText().charAt(0), valueField.getText());
						evolution.reevolve();
						canvas.repaint();
						fillGUI();
					}
				});

				// Layout
				c.gridx = 0;
				c.weightx = 0.1;
				c.fill = GridBagConstraints.HORIZONTAL;
				productionsPanel.add(keyField, c);

				++c.gridx;
				c.weightx = 0.0;
				c.fill = GridBagConstraints.NONE;
				productionsPanel.add(new JLabel("->"), c);

				++c.gridx;
				c.weightx = 1.0;
				c.fill = GridBagConstraints.HORIZONTAL;
				productionsPanel.add(valueField, c);

				++c.gridx;
				c.weightx = 0.0;
				c.fill = GridBagConstraints.HORIZONTAL;
				productionsPanel.add(deleteButton, c);

				++c.gridy;
			}

			JButton addButton = new JButton("Add Production");
			JButton randomButton = new JButton("Randomize");

			addButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					String key = JOptionPane.showInputDialog(null, "What symbol?");
					if (key != null && !key.isEmpty()) {
						lsystem.addProduction(key.charAt(0), key.charAt(0) + "");
						evolution.reevolve();
						canvas.repaint();
						fillGUI();
					}
				}
			});

			randomButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					Character[] keys = new Character[lsystem.getProductionKeys().size()];
					lsystem.getProductionKeys().toArray(keys);
					Random random = new Random();
					for (Character key : keys) {
						int n = random.nextInt(10);
						String value = "";
						for (int i = 0; i < n; ++i) {
							value += keys[random.nextInt(keys.length)];
						}
						lsystem.addProduction(key, value);
					}

					int n = random.nextInt(20);
					String value = "";
					for (int i = 0; i < n; ++i) {
						value += keys[random.nextInt(keys.length)];
					}
					lsystem.setInitialState(value);
					evolution.reevolve();

					fillGUI();
					canvas.repaint();
				}
			});

			++c.gridy;
			c.gridx = 0;
			c.gridwidth = 4;
			c.weightx = 1.0;
			c.fill = GridBagConstraints.HORIZONTAL;
			productionsPanel.add(addButton, c);

			++c.gridy;
			c.gridx = 0;
			c.gridwidth = 4;
			c.weightx = 1.0;
			c.fill = GridBagConstraints.HORIZONTAL;
			productionsPanel.add(randomButton, c);
		}
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				LSystem lsystem = new LSystem("F-F-F-F", 90.0);
				lsystem.addProduction('F', "F-F+F+FF-F-F+F");
				lsystem.addProduction('+', "+");
				lsystem.addProduction('-', "-");
				LSystemViewer frame = new LSystemViewer();
				frame.setLSystem(lsystem);
			}
		});
	}
}