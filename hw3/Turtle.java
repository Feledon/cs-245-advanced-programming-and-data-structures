package hw3;

public class Turtle {
	private double x;
	private double y;
	private double angle;

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getHeading() {
		return angle;
	}

	public void setHeading(double heading) {
		this.angle = heading;
	}

	public Turtle(double x, double y, double angle){
		this.x = x;
		this.y = y;
		this.angle = angle;
	}
	public Turtle(Turtle turtle){
		this.x = turtle.x;
		this.y = turtle.y;
		this.angle = turtle.angle;
	}
}
