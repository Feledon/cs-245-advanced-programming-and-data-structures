package lab09;

public interface KeyValueVisitor<K, V> {
	 public void visit(K key, V value, int level);
}
