package lab09;

import java.io.FileNotFoundException;

public class BinaryTreeTester {
	public static void main(String[] args) throws FileNotFoundException {
		BinarySearchTree<Integer, String> tree = new BinarySearchTree<Integer, String>();

		tree.add(50, "root");
		tree.add(25, "l");
		tree.add(75, "r");
		tree.add(12, "ll");
		tree.add(37, "lr");
		tree.add(62, "rl");
		tree.add(87, "rr");
		tree.add(100, "rrr");

		tree.traversePreorder(new KeyValueVisitor<Integer, String>() {
			@Override
			public void visit(Integer key,String value,int level) {
				System.out.println(getSpace(level) + key + " -> " + value);
			}});
int remove = 75;
System.out.println("Removing : " + remove);
		tree.remove(remove);

		tree.traversePreorder(new KeyValueVisitor<Integer, String>() {
			@Override
			public void visit(Integer key,String value,int level) {
				System.out.println(getSpace(level) + key + " -> " + value);
			}
		});
	}

	private static String getSpace(int nspaces) {
		String s = "";
		for (int i = 0; i < nspaces; ++i) {
			s += " ";
		}
		return s;
	}
}
