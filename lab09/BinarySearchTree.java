package lab09;

import java.util.NoSuchElementException;

public class BinarySearchTree<K extends Comparable<K>, V> {
	private Node<K, V> root;

	public BinarySearchTree() {
		root = null;
	}

	public boolean isEmpty() {
		return root == null;
	}


	public int getHeight(){
		return getHeight(root);
	}

	private int getHeight(Node<K, V> startingNode){
		if(startingNode==null){
			return 0;
		}
		else{
			// return 1 + Math.max(getHeight(startingNode.left), getHeight(startingNode.right))
			int leftBranch = getHeight(startingNode.left) + 1;
			int rightBranch = getHeight(startingNode.right) + 1;
			if(rightBranch > leftBranch){
				return rightBranch;
			}else{
				return leftBranch;
			}			 
		}
	}

	public int size() {
		return size(root);
	}

	private int size(Node<K, V> startingNode) {
		// Base case: when startingNode is null
		if (startingNode == null) {
			return 0;
		}

		// General case: when startingNode is not null
		else {
			return 1 + size(startingNode.left) + size(startingNode.right);
		}
	}

	/**
	 * Remove root's inorder successor from its right subtree.
	 * 
	 * @param root
	 * Node whose inorder successor is to be removed.
	 * @return Root's inorder successor.
	 */
	private Node<K, V> subtreeByRemovingInorderSuccessor(Node<K, V> root) {
		return subtreeByRemovingSmallestDescendent(root, root.right);
	}

	/**
	 * Remove and get the smallest descendant from the subtree starting at child.
	 * The removed node's parent is updated so that it no longer references the
	 * removed node.
	 * 
	 * @param parent
	 * The parent of the subtree, needed so that its left/right links may be
	 * updated
	 * @param child
	 * The root of the subtree from which the smallest node is removed
	 * @return The smallest descendant in the subtree
	 */
	private Node<K, V> subtreeByRemovingSmallestDescendent(Node<K, V> parent,
			Node<K, V> child) {
		// If we can't go left from here, then child is the smallest.
		if (child.left == null) {

			// If the child is the left child of its parent, we must
			// update the parent's left link to route around this node.
			if (parent.left == child) {
				parent.left = child.right;
			}

			// Otherwise, if the child is the right child of its parent,
			// we must update the parent's right link to route around this node.
			else {
				parent.right = child.right;
			}

			return child;
		}

		// Otherwise
		else {
			return subtreeByRemovingSmallestDescendent(child, child.left);
		}
	}

	public V get(K key) {
		return get(key, root);
	}

	private V get(K key,
			Node<K, V> startingNode) {
		if (startingNode == null) {
			throw new NoSuchElementException("No value for key " + key);
		} else {
			int order = key.compareTo(startingNode.key);
			if (order == 0) {
				return startingNode.value;
			} else if (order < 0) {
				return get(key, startingNode.left);
			} else {
				return get(key, startingNode.right);
			}
		}
	}

	public void remove(K key){
		root = subtreeByRemoving(root, key);
	}

	private Node<K, V> subtreeByRemoving(Node<K, V> startingNode, K key){
		Node<K, V> changedTree = startingNode;

		if(startingNode == null){
			throw new NoSuchElementException();
		}

		int order = key.compareTo(startingNode.key);
		if (order < 0){
			changedTree.left = subtreeByRemoving(startingNode.left, key);
		}
		else if (order > 0) {
			changedTree.right = subtreeByRemoving(startingNode.right, key);
		}
		else{
			//destroy
			if(startingNode.left == null && startingNode.right == null){
				changedTree = null;
			}else if(startingNode.left == null){
				changedTree = startingNode.right;
				changedTree.right = null;
			}else if(startingNode.right == null){			
				changedTree = startingNode.left;
				changedTree.left = null;
			}else{
				Node<K, V> newNode = subtreeByRemovingInorderSuccessor(startingNode);
				changedTree.value = newNode.value;
				changedTree.key = newNode.key;
			}
		}
		return changedTree;

	}

	public void add(K key,
			V value) {
		root = subtreeByAdding(root, key, value);
	}

	private Node<K, V> subtreeByAdding(Node<K, V> startingNode,K key,V value) {
		Node<K, V> subtreeRoot;
		// We hit paydirt!
		if (startingNode == null) {
			subtreeRoot = new Node<K, V>(key, value, null, null);
		}
		// The subtree we're trying to insert into is non-empty. A couple
		// of things may be true. The new item might belong in one of this
		// node's subtrees. Or it may match the node itself.
		else {
			int order = key.compareTo(startingNode.key);
			subtreeRoot = startingNode;

			// Thing to insert appears left of pivot point! Blam!
			if (order < 0) {
				startingNode.left = subtreeByAdding(startingNode.left, key, value);
			}

			// Thing to insert appears right of pivot point! Whap!
			else if (order > 0) {
				startingNode.right = subtreeByAdding(startingNode.right, key, value);
			}

			// The keys are the same. Oh oh oh.
			else {
				startingNode.value = value;
			}
		}

		return subtreeRoot;
	}

	public void traversePreorder(KeyValueVisitor<K, V> visitor) {
		traversePreorder(root, visitor, 0);
	}

	private void traversePreorder(Node<K, V> startingNode, KeyValueVisitor<K, V> visitor, int level) {
		if (startingNode != null) {
			visitor.visit(startingNode.key, startingNode.value, level);
			traversePreorder(startingNode.left, visitor, level + 1);
			traversePreorder(startingNode.right, visitor, level + 1);
		}
	}

	private static class Node<K, V> {
		K key;
		V value;

		Node<K, V> left;
		Node<K, V> right;

		public Node(K key,
				V value,
				Node<K, V> left,
				Node<K, V> right) {
			this.key = key;
			this.value = value;
			this.left = left;
			this.right = right;
		}
	}
}