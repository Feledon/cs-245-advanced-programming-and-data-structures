package pre2;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class MagicArray <T>{
	private T[] items;
	private int itemCount;
	private int maxCapacity;

	@SuppressWarnings("unchecked")
	public MagicArray(int capacity) {
		items = (T[]) new Object[capacity];
		itemCount = 0;
		maxCapacity = capacity;
	}

	public T get(int i) {
		if(i > size() || items[i] == null){
			throw new IndexOutOfBoundsException("Index out of bounds.");
		}else{
			return items[i];
		}
	}

	public int size(){
		return itemCount;
	}

	public void add(T s) {
		if (size() >= items.length) {
			maxCapacity = maxCapacity + 5;
			items = Arrays.copyOf(items, maxCapacity);
		}
		items[itemCount] = s;
		++itemCount;
	}

	public void remove(int index){
		if(index < 0 || index > size()|| items[index] == null){
			throw new IndexOutOfBoundsException("Index out of bounds."); 
		}else{
			for(int i = index; i < size(); ++i){
				items[i] = items[i + 1];				
			}
			--itemCount;	
		}
	}

	public void remove(T item){
		boolean found = false;

		for(int i = 0; i < size(); ++i){
			if(items[i].equals(item)){
				remove(i);
				found = true;
				break;
			}
		}

		if(!found){
			throw new NoSuchElementException("Object not present in the collection."); 
		}
	}
}
