package pre2;

public class tester {

	public static void main(String[] args) {
		try {
			MagicArray<String> list = new MagicArray<String>(5);
			list.add("Brad");
			list.add("Mike");
			list.add("Jody");
			list.add("Jamie");
			list.add("Bobbie");
			list.add("Shaun");

			list.remove("Jamie");

			for(int i = 0; i < list.size() - 1; i++){
				System.out.println(list.get(i));
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
