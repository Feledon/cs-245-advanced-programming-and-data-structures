package pre3;

@SuppressWarnings("serial")
public class UndefinedOperatorException extends RuntimeException {
	public UndefinedOperatorException(String undefinedOperator){
		 super("No such operator \"" + undefinedOperator + "\"");
	}
}
