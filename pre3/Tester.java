package pre3;

public class Tester {

	public static void main(String[] args) {
		Expression ex = new Expression("a + a * a");
		ex.addVariable("a", 3);
		ex.addVariable("b", 2);
		double total = 0.0;
		total = ex.evaluate();
			
		System.out.println("Total " + total);
	}
}
