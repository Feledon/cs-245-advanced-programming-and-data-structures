package pre3;

import java.util.HashMap;
import java.util.Scanner;
import java.util.Stack;

public class Expression {
	private Stack<String> stack = new Stack<String>();
	private HashMap<String, Double> numHash = new HashMap<String, Double>();
	private String postFix;

	public static int getPrecedenceLevel(String operator) {
		if (operator.equals("*") || operator.equals("/")) {
			return 2;
		} else if (operator.equals("+") || operator.equals("-")) {
			return 1;
		} else if (operator.equals("(")) {
			return 0;
		} else {
			throw new UndefinedOperatorException(operator);
		}
	}

	public static boolean precedes(String firstOperator, String secondOperator) {
		return firstOperator.equals(secondOperator) || (getPrecedenceLevel(firstOperator) >= getPrecedenceLevel(secondOperator));
	}

	public Expression(String infix) {
		postFix = "";
		for (String s : infix.split(" ")) {
			if (s.matches("-?\\d+(\\.\\d+)?")|| s.matches("[A-z]+")) {
				postFix += s + " ";
			} else {
				if (s.equals("(")) {
					stack.push(s);
				} else if (s.equals(")")) {
					boolean found = false;
					while (!found && !stack.isEmpty()) {
						if (stack.peek().equals("(")) {
							stack.pop();
							found = true;
						} else {
							postFix += stack.pop() + " ";
						}
					}
				} else {
					while (!stack.isEmpty() && precedes(stack.peek(), s)) {
						postFix += stack.pop() + " ";
					}
					stack.push(s);
				}
			}
		}

		while (!stack.isEmpty()) {
			postFix += stack.pop() + " ";
		}
	}

	public void addVariable(String identifier, double value) {
		numHash.put(identifier, value);
	}

	public double evaluate() {
		Scanner in = new Scanner(postFix).useDelimiter(" ");
		Stack<Double> operands = new Stack<Double>();
		while (in.hasNext()) {
			if (in.hasNext("[A-z]+")) {
				String key = in.next();
				if (numHash.containsKey(key)) {
					operands.push(numHash.get(key));
				} else {
					throw new UndefinedVariableException(key);
				}
			}else
			if (in.hasNextDouble()) {
				double number = in.nextDouble();
				operands.push(number);
			} else {
				String operator = in.next();

				Double b = operands.pop();
				Double a = operands.pop();

				if (operator.equals("+")) {
					operands.push(a + b);
				} else if (operator.equals("-")) {
					operands.push(a - b);
				} else if (operator.equals("*")) {
					operands.push(a * b);
				} else if (operator.equals("/")) {
					operands.push(a / b);
				}
			}
		}
		return operands.pop();
	}
}
