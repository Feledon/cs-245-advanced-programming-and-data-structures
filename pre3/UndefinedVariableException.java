package pre3;

@SuppressWarnings("serial")
public class UndefinedVariableException extends RuntimeException {
	public UndefinedVariableException(String varName){
		super("No such variable \"" + varName + "\"");
	}
}
