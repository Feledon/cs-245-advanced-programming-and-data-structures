package lab01;
import junit.framework.Assert;
import org.junit.Test;


public class JUnitTests {

	@Test
	public void testWhiteSpace(){
		String password = "Cs245";

		PasswordMethods pm = new PasswordMethods(password);

		Assert.assertFalse("White Spaces in the Password", pm.hasSpaces());
	}
	
	@Test
	public void testDigits(){
		String password = "Cs245";
		PasswordMethods pm = new PasswordMethods(password);
		Assert.assertEquals(3, pm.digitCount());
		
	}

	@Test
	public void testUpperCaseCount(){
		String password = "Cs245";
		PasswordMethods pm = new PasswordMethods(password);	
		int expected = 1;
		
		Assert.assertEquals(expected, pm.upperCaseCount());		
	}
	
	@Test
	public void testLowerCaseCount(){
		String password = "Cs245";
		PasswordMethods pm = new PasswordMethods(password);
		int expected = 1;
		
		Assert.assertEquals(expected, pm.lowerCaseCount());			
	}
	
	@Test
	public void testEVERYTHING(){
		String password = "Cs245";
		PasswordMethods pm = new PasswordMethods(password);
		
		
		Assert.assertTrue("HAVE YOU NEVER DONE THIS B4??", pm.isValidPassword());			
	}

}
