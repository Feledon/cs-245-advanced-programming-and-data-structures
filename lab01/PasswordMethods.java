package lab01;

public class PasswordMethods {
	private String pw;

	public PasswordMethods(String pass){
		pw = pass;
	}

	public boolean hasSpaces(){

		if (pw.contains(" ")){
			return true;
		}
		
		return false;
	}
	
	public int digitCount(){
		char[] passArray = pw.toCharArray();
		int i;
		int numberCount = 0;
		for (i=0; i<pw.length();i++){
			if (Character.isDigit(passArray[i])){
				numberCount++;
			}
		}
		return numberCount;
	}

	public int upperCaseCount(){
		char[] passArray = pw.toCharArray();
		int i;
		int upperCount = 0;
		
		for (i=0; i<pw.length();i++){
			if (Character.isUpperCase(passArray[i])){
				upperCount++;
			}
		}
		return upperCount;
		
	}
	
	public int lowerCaseCount(){
		char[] passArray = pw.toCharArray();
		int i;
		int upperCount = 0;
		
		for (i=0; i<pw.length();i++){
			if (Character.isLowerCase(passArray[i])){
				upperCount++;
			}
		}
		return upperCount;
		
	}
	
	public boolean isValidPassword(){
		char[] passArray = pw.toCharArray();
		int i;
		int upperCount = this.upperCaseCount();
		int numberCount = this.digitCount();
		int lowerCOunt = this.lowerCaseCount();
		boolean bad=false;
		
		for (i=0; i<pw.length();i++){
			
			if (!Character.isLetterOrDigit(passArray[i])){
				bad = true;
			}
			
		}
		if(numberCount< 2 || upperCount<1 || lowerCOunt<1 || bad==true){
			return false;
		}
		else {
			return true;
		}
		
	}
	
}
