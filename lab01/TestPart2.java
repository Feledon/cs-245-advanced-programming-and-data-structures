package lab01;
import junit.framework.Assert;

import org.junit.Test;

public class TestPart2 {
	@Test
	public void testReverse(){
		String word = "Cs245";
		Part2 pm = new Part2(word);
		String expected = pm.reverse();
		Assert.assertEquals("542sC", expected);
	}
}
