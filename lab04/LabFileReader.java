package lab04;

import java.io.File;

public class LabFileReader {

	public static void main(String[] args) {
		//		listLarge("C:\\Altera");
		listImages("H:\\Documents\\My Pictures\\Pictures");
	}


	/**
	 * Write a method named listImages that accepts a path to a directory as a parameter. 
	 * It prints to the console all files in the directory that are JPEGs, PNGs, BMPs, or GIFs.
	 * @param imagePath
	 */
	public static void listImages(String path){
		File dir = new File(path);		

		getFileLists(dir);

	}

	private static void getFileLists(File dir){
		printFiles(dir.listFiles(new ImageFileFilter()));

		for(File file : dir.listFiles()){
			if(file.isDirectory()){
				getFileLists(file);
			}
		}
	}

	private static void printFiles(File[] fileList){		
		if(fileList!=null){
			for(int i =0; i<fileList.length;i++){
				System.out.println(fileList[i].getName());
			}
		}
	}

}
