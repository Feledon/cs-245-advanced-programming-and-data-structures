package lab04;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class BracketMaker extends JPanel{
	private int ySpacer = 100;
	private int xSpacer = 65;

	public BracketMaker(int i){
	}


	@Override
	public void paintComponent(Graphics g) {				
		drawBracket(g, new ArrayList<Point>());
	}

	private void drawBracket(Graphics g, ArrayList<Point> points){
		if(points.isEmpty()){//No points created, make first one
			g.setColor(Color.PINK);
			g.drawLine(0,350,50,350);	
			points.add(new Point(50,350));			

		}else{//We have points, use them to create the next set and delete the old ones
			ArrayList<Point> newPoints = new ArrayList<Point>();

			for(Point point: points){
				Point tempUpPoint = new Point(point.x+xSpacer, point.y+ySpacer);
				Point tempDownPoint = new Point(point.x+xSpacer, point.y-ySpacer);
				
				if(points.size()<2){
					tempUpPoint.y = tempUpPoint.y + 100;
					tempDownPoint.y = tempDownPoint.y - 100;
				}else if(points.size()<5){
					tempUpPoint.y = tempUpPoint.y + 50;
					tempDownPoint.y = tempDownPoint.y - 50;
				}

				newPoints.add(tempUpPoint);
				newPoints.add(tempDownPoint);

				g.setColor(Color.RED);
				g.drawLine(point.x, point.y,tempUpPoint.x - 35,tempUpPoint.y);
				g.drawLine(tempUpPoint.x - 35,tempUpPoint.y,tempUpPoint.x,tempUpPoint.y);

				g.setColor(Color.BLUE);
				g.drawLine(point.x, point.y,tempDownPoint.x - 35,tempDownPoint.y);
				g.drawLine(tempDownPoint.x-	35,tempDownPoint.y,tempDownPoint.x,tempDownPoint.y);
			}


			if(ySpacer>15){
				ySpacer=ySpacer/(points.size());
				xSpacer-=1;
			}
			points.clear();
			points=newPoints;
		}

		if(points.size() < 150){
			drawBracket(g, points);
		}
	}
}//End Class
