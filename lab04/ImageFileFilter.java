package lab04;

import java.io.File;
import java.io.FileFilter;

public class ImageFileFilter implements FileFilter {

	@Override
	public boolean accept(File pathname) {
			
		if(pathname.isDirectory()){
			return false;
		}else if(pathname.getName().endsWith("jpg") ||pathname.getName().endsWith("png") ||pathname.getName().endsWith("bmp")||pathname.getName().endsWith("gif")){
			return true;			
		}else{
			return false;	
		}		
	}

}
