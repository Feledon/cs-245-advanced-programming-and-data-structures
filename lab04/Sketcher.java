package lab04;

import java.awt.BorderLayout;
import javax.swing.JFrame;

public class Sketcher {
  public static void main(String[] args) {
    JFrame frame = new JFrame("Brackets");
    
    frame.add(new BracketMaker(frame.getHeight()), BorderLayout.CENTER);
    
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setSize(700, 700);
    frame.setVisible(true);
  }
}